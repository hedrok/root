#include <arith.hpp>
#include <io.hpp>

int main()
{
    int a = CompA::IO::getNumber("A");
    int b = CompA::IO::getNumber("B");
    int sum = CompB::Arith::add(a, b);
    CompA::IO::outputNumber("sum", sum);
}
